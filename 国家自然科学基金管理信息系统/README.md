# 国家自然科学基金管理信息系统采集

## 环境介绍
### 浏览器及驱动
Firefox **82.0.3**

geckodriver **0.28.0**

### python模块
Selenium    **3.141.0**

pytesseract **0.3.6**

pillow    **7.2.0**

## geckodriver 环境配置
### geckodriver 版本对应关系

[链接地址](https://firefox-source-docs.mozilla.org/testing/geckodriver/Support.html)

### geckodriver下载地址

[下载地址](https://github.com/mozilla/geckodriver/releases/latest)

## tessearct 环境配置
[环境配置](https://www.cnblogs.com/chenshengkai/p/11318272.html)

## 运行方式
`pip install -r requirements.txt`

`python code.py`